# Console File Sender

README / 07 June 2019

-----

## Introduction

Console File Sender is a simple console-based application for transferring files over TCP protocol.

## Technical information

Name | Value
--- | ---
Main programming language | C#
Framework used | .NET Framework 4.7

## Getting started

Download the [1.0 version](https://gitlab.com/Prologh/console-file-sender/tags/v1.0) from GitLab or clone the project and build it on your own.

## How to use

### Recieving

 1. Tell the app on what IP address you would like to listen:

```
Enter IP adress for listener:
162.23.211.142
```

 2. Tell the app on what port you would like to listen:

```
Enter port number for listener:
8080
```

 3. That's it! Now, let's send some files!

### Sending

 1. Tell the app which file you would like to send. 

###### Note! Application looks for files only in the local directory, that is a directory where the executable file is stored. Absolute paths are not supported.

```
Enter the name of file from local directory you would like to send:
image.jpg
```

 2. Tell the app to what remote IP address you would like to send:

```
Enter IP adress of remote host:
162.23.211.142
```

 3. Tell the app to what remote port you would like to send:

```
Enter port number of remote host:
8080
```

 4. That's it! File should be sent. Application should generate similar output:

```
Trying to connect to [127.0.0.1:8080]
Connected to [127.0.0.1:8080] through local port 50136
Starting to send a file.
Sent 30208 bytes.
File sent successfully.
Connection closed.
```

## License

Licensed under MIT. Read full license [here](https://gitlab.com/Prologh/console-file-sender/blob/master/LICENSE).

## Credits

**Piotr Wosiek** | [GitLab](https://gitlab.com/Prologh) | [GitHub](https://github.com/Prologh)

### Acknowledgements

Project icon based on one made by [Freepik](https://www.freepik.com/) from [www.flaticon.com](https://www.flaticon.com/).
