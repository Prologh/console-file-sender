﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace TcpFileSender.ConsoleApp
{
    public class FileReciever
    {
        public void BeginRecieveFiles(IPEndPoint localIPEndPoint)
        {
            var listener = new TcpListener(localIPEndPoint);
            listener.Start();

            Console.WriteLine("\nStarted to listen on [{0}]\n", localIPEndPoint);

            while (true)
            {
                using (var client = listener.AcceptTcpClient())
                {
                    Console.WriteLine("Connected with with [{0}]", client.Client.RemoteEndPoint);
                    using (var stream = client.GetStream())
                    {
                        using (var output = File.Create(GetNextFreeFilePath()))
                        {
                            Console.WriteLine("Starting to receive the file.");

                            // Read the file in chunks of 1KB.
                            var buffer = new byte[1024];
                            var bytesSum = 0L;
                            int bytesRead;
                            while ((bytesRead = stream.Read(buffer, 0, buffer.Length)) > 0)
                            {
                                output.Write(buffer, 0, bytesRead);
                                bytesSum += bytesRead;
                                if (bytesSum % 1048576 == 0)
                                {
                                    Console.Write("\rReceiving {0} bytes          ", bytesSum);
                                }
                            }
                            ClearCurrentConsoleLine();
                            Console.WriteLine("Received {0} bytes.", bytesSum);

                            Console.WriteLine("File recieved successfully.");
                            Console.WriteLine("Connection closed.\n");
                        }
                    }
                }
            }
        }

        private string GetNextFreeFilePath()
        {
            string currentDirectory = Directory.GetCurrentDirectory();
            var random = new Random();
            string filePath;
            bool exists;
            do
            {
                filePath = currentDirectory + "\\" + "received-file-" + random.Next(1000);
                exists = File.Exists(filePath);
            } while (exists);

            return filePath;
        }

        private static void ClearCurrentConsoleLine()
        {
            int currentLineCursor = Console.CursorTop;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, currentLineCursor);
        }
    }
}
