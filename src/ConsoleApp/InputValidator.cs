﻿using System;
using System.IO;
using System.Net;

namespace TcpFileSender.ConsoleApp
{
    public static class InputValidator
    {
        public static string ValidateFilePath()
        {
            string filePath;
            bool exists = true;
            do
            {
                if (!exists)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("File with specified name does not exist!");
                    Console.ResetColor();
                }

                filePath = Directory.GetCurrentDirectory() + "\\" + Console.ReadLine();
                exists = File.Exists(filePath);
            } while (!exists);

            return filePath;
        }

        public static IPAddress ValidateIPAdress()
        {
            IPAddress ipAddress;
            bool valid = true;
            do
            {
                if (!valid)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Invalid IP address!");
                    Console.ResetColor();
                }

                valid = IPAddress.TryParse(Console.ReadLine(), out ipAddress);
            } while (!valid);

            return ipAddress;
        }

        public static int ValidatePortNumber()
        {
            int port;
            bool valid = true;
            do
            {
                if (!valid)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Invalid port number!");
                    Console.ResetColor();
                }

                valid = int.TryParse(Console.ReadLine(), out port);
            } while (!valid || port < IPEndPoint.MinPort || port > IPEndPoint.MaxPort);

            return port;
        }
    }
}
