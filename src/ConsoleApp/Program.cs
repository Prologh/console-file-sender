﻿using System;
using System.IO;
using System.Net;

namespace TcpFileSender.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Choose what you would like to do:\n");
            Console.WriteLine("\t1) recieve files");
            Console.WriteLine("\t2) send a file");
            Console.WriteLine("\t3) close program");

            char key;
            do
            {
                key = Console.ReadKey(intercept: true).KeyChar;
            } while (key != '1' && key != '2' && key != '3');

            Console.Clear();

            switch (key)
            {
                case '1':
                    RecieveFiles();
                    break;
                case '2':
                    SendFilesLoop();
                    break;
                case '3':
                    Environment.Exit(0);
                    return;
            }

            Console.WriteLine("\nPress any key to close the program...");
            Console.ReadKey(intercept: true);
        }

        static void RecieveFiles()
        {
            Console.WriteLine("Enter IP adress for listener:");
            var ipAddress = InputValidator.ValidateIPAdress();

            Console.WriteLine("Enter port number for listener:");
            var portNumber = InputValidator.ValidatePortNumber();

            new FileReciever().BeginRecieveFiles(new IPEndPoint(ipAddress, portNumber));
        }

        static void SendFilesLoop()
        {
            char key;
            do
            {
                try
                {
                    SendFile();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.StackTrace);
                };
                Console.WriteLine("\nWould like to send another file? (Y/N):");
                key = Console.ReadKey(intercept: true).KeyChar;
            } while (char.ToUpper(key) == 'Y');
        }

        static void SendFile()
        {
            Console.WriteLine("Enter the name of file from local directory you would like to send:");
            var filePath = InputValidator.ValidateFilePath();

            Console.WriteLine("Enter IP adress of remote host:");
            var ipAddress = InputValidator.ValidateIPAdress();

            Console.WriteLine("Enter port number of remote host:");
            var portNumber = InputValidator.ValidatePortNumber();

            new FileSender().SendFile(filePath, new IPEndPoint(ipAddress, portNumber));
        }
    }
}
