﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace TcpFileSender.ConsoleApp
{
    public class FileSender
    {
        public void SendFile(string filePath, IPEndPoint remoteIPEndPoint)
        {
            if (string.IsNullOrEmpty(filePath))
            {
                throw new ArgumentException(
                    $"{nameof(filePath)} cannot be null, empty " +
                    $"or contain only whitespace characters.",
                    nameof(filePath));
            }

            var socket = new Socket(SocketType.Stream, ProtocolType.Tcp);
            Console.WriteLine("\nTrying to connect to [{0}]", remoteIPEndPoint);
            socket.Connect(remoteIPEndPoint);
            Console.WriteLine("Connected to [{0}] through local port {1}", remoteIPEndPoint, ((IPEndPoint)socket.LocalEndPoint).Port);

            Console.WriteLine("Starting to send a file.");

            using (var fileStream = File.OpenRead(filePath))
            {
                // Send the file in chunks of 1KB.
                var buffer = new byte[1024];
                var bytesSum = 0L;
                int bytesRead;
                while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    socket.Send(buffer, bytesRead, SocketFlags.None);
                    bytesSum += bytesRead;
                    if (bytesSum % 1048576 == 0)
                    {
                        Console.Write("\rSending {0} bytes          ", bytesSum);
                    }
                }
                ClearCurrentConsoleLine();
                Console.WriteLine("Sent {0} bytes.", bytesSum);
            }

            //socket.SendFile(filePath);

            Console.WriteLine("File sent successfully.");
            Console.WriteLine("Connection closed.\n");
            socket.Close();
        }

        private static void ClearCurrentConsoleLine()
        {
            int currentLineCursor = Console.CursorTop;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, currentLineCursor);
        }
    }
}
